<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'QI&i>CrZ9w)urv{~:9.;!sA%ZSj)QH_!y|7.79-,6|EF%.|M;Y-[#TpIrc#^9v}a');
define('SECURE_AUTH_KEY',  'QRBNBi72EzQylX66~|{L3hw}s!I0e[UEMb;[[bZ%UzbhpP BVwWp4Elzm|o7vPAM');
define('LOGGED_IN_KEY',    '#eV nd^]I&<V`Vk!?vIymr[M)*i3)} >G@@|T0idm7?siGm%3u*CX6=fHuTKWnyj');
define('NONCE_KEY',        '1-vUPwdOF!d{?M~}0H^YB< Gje1cA}?nbkb&9@U2]ZngU}(h?9WZ~L2?|0ozM3:R');
define('AUTH_SALT',        '8OV(/J{?)]^8qH|0OjEnR$!P0.0DK32V|wF_6!hw:lFe5x/oAZ/HhdF5yPivur%n');
define('SECURE_AUTH_SALT', 'kyP?z:q+LQRpGqjbhU@x>~piHv~3JLz)DhA(9rHzrk.HEljqG<}t D?4AS),%O4c');
define('LOGGED_IN_SALT',   'V/QQ4A:^N6R:R6]O41.Z)*`%vx<:d_tIudi}oZ%{}g,.nWwE$#w6T@[# kent}*D');
define('NONCE_SALT',       'waBIqy_cr.]m]X>-k&V__AlX_bVwba(=~ev 8Wz=9/WAL]VoC&5{v-&2ohJdI8c6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
